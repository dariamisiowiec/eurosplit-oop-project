package fcul.pco.eurosplit.main;

import fcul.pco.eurosplit.domain.*;
import fcul.pco.eurosplit.domain.utils.Data;
import fcul.pco.eurosplit.domain.utils.Table;

import java.io.IOException;
import java.util.*;

/**
 * @author tl
 */
public class Interp {

    /**
     * Contains the string that is correspond to interpreter's prompt. It is
     * printed on the console. The prompt is updated by the setPrompt() method.
     */
    private String prompt;
    /**
     * The input of the interpreter
     */
    private final Scanner input;
    /**
     * Contains the current user (after user creation or after login).
     */
    private User currentUser;

    /**
     * Contains the current Split
     */
    private Split currentSplit;

    /**
     * @param input
     */
    public Interp(Scanner input) {
        prompt = ApplicationConfiguration.DEFAULT_PROMPT;
        this.input = input;
    }

    /**
     * Main interpreter command
     *
     * @param command
     * @param input
     */
    public void execute(String command, Scanner input) {
        switch (command) {
            case "help":
                help();
                break;
            case "new user":
                makeNewUser(input);
                break;
            case "show users":
                showUsers();
                break;
            case "login":
                login(input);
                break;
            case "new split":
                makeNewSplit(input);
                break;
            case "select split":
                selectSplit(input);
                break;
            case "new expense":
                makeNewExpense(input);
                break;
            case "balance":
                printBalance();
                break;
            case "quit":
                quit();
                break;
            default:
                System.out.println("Unknown command. [" + command + "]");
        }
    }

    private void help() {
        System.out.println("help: show commands information.");
        System.out.println("new user: create a new account.");
        System.out.println("show users: show the list of registred users.");
        System.out.println("new split: create a new split.");
        System.out.println("select split: select a split.");
        System.out.println("new expense: add an expense to current split.");
        System.out.println("balance: print the balance of the current split.");
        System.out.println("login: log a user in.");
        System.out.println("quit: terminate the program.");
    }

    private void makeNewUser(Scanner input) {
        System.out.print("User name: ");
        String name = input.nextLine();
        System.out.print("Email address: ");
        String email = input.nextLine();
        if (Start.getUserCatalog().hasUserWithId(email)) {
            System.out.println("A user with this email address already exists.");
            currentUser = Start.getUserCatalog().getUserById(email);
            setPrompt();
        } else {
            Start.getUserCatalog().addUser(new User(name, email));
            currentUser = new User(name, email);
            setPrompt();
        }
    }

    private void quit() {
        save();
    }

    private void showUsers() {
        if(Start.getUserCatalog().getUsers().isEmpty()){
            System.out.println("There are no users yet.");
            return;
        }
        System.out.println(Table.tableToString(Start.getUserCatalog().getUsers()));
    }

    private void login(Scanner input) {
        System.out.print("Username: ");
        String username = input.nextLine();
        if (!UserCatalog.getUserCatalog().hasUserWithName(username)) {
            System.out.println("User [" + username + "] does not exist");
            return;
        }
        System.out.print("Email: ");
        String email = input.nextLine();
        if (!UserCatalog.getUserCatalog().hasUserWithId(email)) {
            System.out.println("User with email [" + email + "] does not exist");
            return;
        }
        if (!UserCatalog.getUserCatalog().getUserById(email).getName().toLowerCase().equals(username.toLowerCase())) {
            System.out.println("Username and email does not match");
            return;
        }
        currentUser = Start.getUserCatalog().getUserById(email);
        setPrompt();
    }


    private void makeNewSplit(Scanner input) {
        if (currentUser == null) {
            System.out.println("You need to be logged in to make a new split");
            return;
        }
        System.out.println("What event do you want to associate with the split?");
        String event = input.nextLine();
        Split split = new Split(currentUser, event);
        currentSplit = split;
        SplitCatalog.getSplitCatalog().addSplit(currentUser, split);
        setPrompt();
    }

    private void selectSplit(Scanner input) {
        if (currentUser == null) {
            System.out.println("You need to be logged in to select a split");
            return;
        }
        System.out.println("Whose split do you want to choose?");
        String ownerName = input.nextLine();
        User splitOwner = selectUser(input, ownerName);
        Split selectedSplit = selectOwnersSplit(input, splitOwner);
        if (selectedSplit != null) {
            currentSplit = selectedSplit;
            setPrompt();
        }

    }

    private Split selectOwnersSplit(Scanner input, User splitOwner) {
        List<Split> userSplits = SplitCatalog.getSplitCatalog().getUserSplits(splitOwner);
        if (userSplits.isEmpty()) {
            System.out.println(splitOwner.getName() + " has no splits yet.");
            return null;
        }
        for (int i = 1; i <= userSplits.size(); i++) {
            System.out.println(i + " - " + userSplits.get(i-1).getPurpose());
        }

        System.out.println("Select the split number");
        int splitIndex = Integer.parseInt(input.nextLine());
        if (splitIndex >= 1 && splitIndex <= userSplits.size()) {
            return userSplits.get(splitIndex - 1);
        } else {
            System.out.println("There is no split with this index");
            return selectOwnersSplit(input, splitOwner);
        }
    }


    private void printBalance() {
        if (currentSplit == null) {
            System.out.println("You need to select a split first");
        }
        HashMap<User, Integer> userAmountHashmap = new HashMap<>();
        for (Expense expense : currentSplit.getExpenses()) {
            Integer expenseValue = expense.getValue();
            Integer expenseValuePerPerson = Math.floorDiv(expenseValue, expense.getNumberOfUsersSharingExpense());
            Integer reminder = expenseValue - (expenseValuePerPerson * expense.getNumberOfUsersSharingExpense());
            User paidBy = expense.getPaidBy();
            List<User> paidFor = expense.getPaidFor();

            if (!userAmountHashmap.containsKey(paidBy)) {
                userAmountHashmap.put(paidBy, expenseValue - expenseValuePerPerson);
            } else {
                userAmountHashmap.put(paidBy, userAmountHashmap.get(paidBy) + expenseValue - expenseValuePerPerson);
            }

            for (User userPaidFor : paidFor) {
                if (!userAmountHashmap.containsKey(userPaidFor)) {
                    userAmountHashmap.put(userPaidFor, -1 * expenseValuePerPerson);
                } else {
                    userAmountHashmap.put(userPaidFor, userAmountHashmap.get(userPaidFor) - expenseValuePerPerson);
                }
            }

            if (reminder > 0) {
                List<User> allUsers = new ArrayList<>();
                allUsers.add(paidBy);
                allUsers.addAll(paidFor);
                List<User> randomlySelectedUsers = chooseRandomlyXUsers(reminder, allUsers);
                for (User user : userAmountHashmap.keySet()) {
                    if (randomlySelectedUsers.contains(user)) {
                        userAmountHashmap.put(user, userAmountHashmap.get(user) - 1);
                    }
                }
            }
        }
        List<List<String>> listForPrinting = new ArrayList<>();
        for (User user : userAmountHashmap.keySet()) {
            List<String> line = new ArrayList<>();
            line.add(user.getName());
            line.add(userAmountHashmap.get(user).toString());
            listForPrinting.add(line);
        }

        if (!listForPrinting.isEmpty()) {
            System.out.println(Table.tableToString(listForPrinting));
        } else {
            System.out.println("There are no expenses associated with this split");
        }

    }

    private List<User> chooseRandomlyXUsers(int x, List<User> users) {
        Random random = new Random();
        List<User> randomlySelectedUsers = new ArrayList<>();
        List<Integer> alreadySelectedNumbers = new ArrayList<>();
        int randomIndex;
        while (randomlySelectedUsers.size() < x) {
            do {
                randomIndex = random.nextInt(users.size());
            } while (alreadySelectedNumbers.contains(randomIndex));
            randomlySelectedUsers.add(users.get(randomIndex));
            alreadySelectedNumbers.add(randomIndex);
        }
        return randomlySelectedUsers;
    }


    private void save() {
        try {
            Start.getUserCatalog().save();
        } catch (IOException ex) {
            System.err.println("Error saving User Catalog.");
        }
        try {
            Start.getExpenseCatalog().save();
        } catch (IOException ex) {
            System.err.println("Error saving Expense Catalog.");
        }
        try {
            Start.getSplitCatalog().save();
        } catch (IOException ex) {
            System.err.println("Error saving Split Catalog");
        }
    }


    private void makeNewExpense(Scanner input) {
        if (currentUser == null) {
            System.out.println("You need to be logged in to make a new expense");
            return;
        }
        if (currentSplit == null) {
            System.out.println("You need to select a split firstly");
            return;
        }
        System.out.println("Expense made by you (" + currentUser + ") What did you pay for ?");
        String purpose = input.nextLine();
        String stringValue;
        do {
            System.out.println("How much did you pay?");
            stringValue = input.nextLine();
        } while (!isInteger(stringValue));
        Integer value = Integer.parseInt(stringValue);
        Expense expense = new Expense(purpose, value, currentUser, Data.now());
        String paidFor = "";
        while (!paidFor.equals("no one")) {
            System.out.println("Who did you pay for: («no one» to terminate)");
            paidFor = input.nextLine();
            if (paidFor.equals("no one")) {
                break;
            }
            User paidForUser = selectUser(input, paidFor);
            expense.addPaidFor(paidForUser);
        }
        currentSplit.addExpense(expense);
        ExpenseCatalog.getExpenseCatalog().addExpense(expense);
    }

    private boolean isInteger(String input) {
        try {
            int x = Integer.parseInt(input);
        } catch (NumberFormatException nf) {
            return false;
        }
        return true;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt() {
        if (currentUser == null) {
            this.prompt = ApplicationConfiguration.DEFAULT_PROMPT;
        } else if (currentSplit == null) {
            this.prompt = currentUser.getName();
        } else {
            this.prompt = currentUser.getName() + "." + currentSplit.getPurpose();
        }
    }

    String nextToken() {
        String in;
        System.out.print(prompt + "> ");
        System.out.flush();
        if (input.hasNextLine()) {
            in = input.nextLine();
            return in;
        } else {
            return "";
        }

    }

    private User selectUser(Scanner input, String name) {
        List<User> list = Start.getUserCatalog().getUsersWithName(name);
        if (list.size() == 1) {
            return list.get(0);
        }
        int k;
        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(i + " " + list.get(i));
            }
            System.out.print("Select a user: ");
            k = Integer.parseInt(input.nextLine());
            if (k >= 0 && k < list.size()) {
                return list.get(k);
            } else {
                System.out.println("The user with this index does not exist");
                return selectUser(input, name);
            }
        } else {
            System.out.println("User not found.");
            System.out.print("Name: ");
            name = input.nextLine();
            return selectUser(input, name);
        }
    }

    /**
     * This method may be used to find a user in the catalog given its name, for
     * example when we want to add "paidFor" users to an expense. The method
     * receives a name. If there is only one user with this name, return that
     * user. If there is no user with that name, give the opportunity to create
     * a new user. The several users (with same name) are found, show the list
     * and ask which one should be used.
     *
     * @param input
     * @param name
     * @return
     */
    private User selectOrCreateUser(Scanner input, String name) {
        ArrayList<User> list = Start.getUserCatalog().getUsersWithName(name);
        if (list.isEmpty()) {
            System.out.println("There is no registred user with name " + name + ".");
            if (askYNQuestion(input, "Do you want to create user " + name)) {
                User theUser = currentUser;
                //   makeNewUser(input, name); <-- write this method
                User newUser = currentUser;
                currentUser = theUser;
                setPrompt();
                return newUser;
            } else {
                // ask again:
                System.out.println("pff... Who did you pay for: ");
                return selectOrCreateUser(input, input.nextLine());
            }
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            int i = 0;
            for (User u : list) {
                System.out.print("(" + i + ") " + u.getName() + "[" + u.getEmail() + "] - ");
                i++;
            }
            System.out.println("Which " + name + " ? ");
            i = input.nextInt();
            return list.get(i);
        }
    }

    private boolean askYNQuestion(Scanner input, String question) {
        System.out.print(question + "? (y/n):");
        String answer = input.nextLine();
        while (!(answer.equalsIgnoreCase("Y")
                || answer.equalsIgnoreCase("N"))) {
            System.out.print(question + "? (y/n):");
            answer = input.nextLine();
        }
        return answer.equalsIgnoreCase("Y");
    }

}