package fcul.pco.eurosplit.main;

import fcul.pco.eurosplit.domain.ExpenseCatalog;
import fcul.pco.eurosplit.domain.SplitCatalog;
import fcul.pco.eurosplit.domain.UserCatalog;

import java.io.IOException;
import java.util.Scanner;

public class Start {

    private static UserCatalog userCatalog;
    private static ExpenseCatalog expenseCatalog;
    private static SplitCatalog splitCatalog;

    private static void deleteCatalogs() {
        userCatalog = null;
        expenseCatalog = null;
        splitCatalog = null;
    }

    public static UserCatalog getUserCatalog() {
        return userCatalog;
    }

    public static ExpenseCatalog getExpenseCatalog() {
        return expenseCatalog;
    }

    public static SplitCatalog getSplitCatalog() {
        return splitCatalog;
    }

    public static void initialize() {
        try {
            userCatalog = UserCatalog.getUserCatalog();
            userCatalog.load();
        } catch (IOException ioe) {
            System.out.println("Could not load userCatalog");
        }
        try {
            expenseCatalog = ExpenseCatalog.getExpenseCatalog();
            expenseCatalog.load();
        } catch (IOException ioe) {
            System.out.println("Could not load expenseCatalog");
        }
        try {
            splitCatalog = SplitCatalog.getSplitCatalog();
            splitCatalog.load();
        } catch (IOException ioe) {
            System.out.println("Could not load splitCatalog");
        }
    }

    private static void run() {
        deleteCatalogs();
        Scanner input = new Scanner(System.in);
        initialize();
        Interp interp = new Interp(input);
        String command = "";
        do {
            command = interp.nextToken();
            interp.execute(command, input);
        } while (!command.equals("quit"));
    }


    public static void main(String[] args) {
        run();
    }

}
