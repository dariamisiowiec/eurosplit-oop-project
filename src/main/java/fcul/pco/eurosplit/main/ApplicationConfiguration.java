package fcul.pco.eurosplit.main;

public class ApplicationConfiguration {

    public static final String ROOT_DIRECTORY = "./data/";
    public static String USER_CATALOG_FILENAME = "usersCatalog";
    public static String SPLIT_CATALOG_FILENAME = "splitCatalog";
    public static String EXPENSES_CATALOG_FILENAME = "expensesCatalog";
    static String DEFAULT_PROMPT = "EuroSplit";

}
