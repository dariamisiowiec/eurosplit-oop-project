package fcul.pco.eurosplit.persistance;

import fcul.pco.eurosplit.domain.Expense;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static fcul.pco.eurosplit.domain.Expense.EXPENSE_SEPARATOR;
import static fcul.pco.eurosplit.main.ApplicationConfiguration.EXPENSES_CATALOG_FILENAME;
import static fcul.pco.eurosplit.main.ApplicationConfiguration.ROOT_DIRECTORY;

public class ExpenseCatalogDA {

    public static void save(Map<Integer, Expense> expenses) throws IOException {
        File file = new File(ROOT_DIRECTORY + EXPENSES_CATALOG_FILENAME);
        FileWriter fileWriter = new FileWriter(file);
        for (Integer key : expenses.keySet()) {
            fileWriter.write(expenses.get(key).toString() + String.format("%n"));
        }
        fileWriter.close();
    }

    public static Map<Integer, Expense> load(){
        HashMap<Integer, Expense> expenseCatalog = new HashMap<>();
        try {
            File file = new File(ROOT_DIRECTORY + EXPENSES_CATALOG_FILENAME);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String expense = scanner.nextLine();
                expenseCatalog.put(Integer.parseInt(expense.split(EXPENSE_SEPARATOR)[0]), Expense.fromString(expense));
            }
            scanner.close();
        }catch (FileNotFoundException e){

        }
        return expenseCatalog;
    }
}
