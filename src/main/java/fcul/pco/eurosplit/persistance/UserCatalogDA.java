package fcul.pco.eurosplit.persistance;

import fcul.pco.eurosplit.domain.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static fcul.pco.eurosplit.main.ApplicationConfiguration.ROOT_DIRECTORY;
import static fcul.pco.eurosplit.main.ApplicationConfiguration.USER_CATALOG_FILENAME;

public class UserCatalogDA {
    public static void save(Map<String, User> users) throws IOException {
        File file = new File(ROOT_DIRECTORY + USER_CATALOG_FILENAME);
        FileWriter fileWriter = new FileWriter(file);
        for (String key : users.keySet()) {
            fileWriter.write(users.get(key).toString() + String.format("%n"));
        }
        fileWriter.close();
    }

    public static Map<String, User> load(){
        HashMap<String, User> userCatalog = new HashMap<String, User>();
        try {
            File file = new File(ROOT_DIRECTORY + USER_CATALOG_FILENAME);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String user = scanner.nextLine();
                userCatalog.put(user.split("/")[1], User.fromString(user));
            }
            scanner.close();
        } catch (FileNotFoundException e){

        }
        return userCatalog;
    }


}
