package fcul.pco.eurosplit.persistance;

import fcul.pco.eurosplit.domain.Split;
import fcul.pco.eurosplit.domain.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static fcul.pco.eurosplit.main.ApplicationConfiguration.ROOT_DIRECTORY;
import static fcul.pco.eurosplit.main.ApplicationConfiguration.SPLIT_CATALOG_FILENAME;

public class SplitCatalogDA {

    public static void save(Map<User, List<Split>> splits) throws IOException {
        File file = new File(ROOT_DIRECTORY + SPLIT_CATALOG_FILENAME);
        FileWriter fileWriter = new FileWriter(file);
        for (User key : splits.keySet()) {
            for (Split split : splits.get(key)) {
                fileWriter.write(split.toString() + String.format("%n"));
            }
        }
        fileWriter.close();
    }

    public static Map<User, List<Split>> load() {
        HashMap<User, List<Split>> splitCatalog = new HashMap<>();
        try {
            File file = new File(ROOT_DIRECTORY + SPLIT_CATALOG_FILENAME);
            Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String splitEntry = scanner.nextLine();
            Split split = Split.fromString(splitEntry);
            User owner = split.getOwner();
            if (!splitCatalog.containsKey(owner)) {
                splitCatalog.put(owner, new ArrayList<>());
            }
            splitCatalog.get(owner).add(split);
        }
        scanner.close();
        } catch (FileNotFoundException e){

        }
        return splitCatalog;
    }

}
