package fcul.pco.eurosplit.domain;

import fcul.pco.eurosplit.persistance.SplitCatalogDA;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplitCatalog {

    public static SplitCatalog splitCatalog = null;
    private Map<User, List<Split>> splits;

    private SplitCatalog() {
        splits = new HashMap<>();
    }

    public static SplitCatalog getSplitCatalog() {
        if (splitCatalog == null) {
            splitCatalog = new SplitCatalog();
        }
        return splitCatalog;
    }

    public void addSplit(User user, Split split) {
        if (!splits.containsKey(user)) {
            splits.put(user, new ArrayList<>());
        }
        splits.get(user).add(split);
    }

    public void save() throws IOException {
        SplitCatalogDA.save(splits);
    }

    public void load() throws IOException {
        splits = SplitCatalogDA.load();
    }

    public List<Split> getUserSplits(User currentUser) {
        if (splits.containsKey(currentUser)) {
            return splits.get(currentUser);
        }
        return new ArrayList<Split>();
    }

}
