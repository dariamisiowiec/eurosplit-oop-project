package fcul.pco.eurosplit.domain;

import fcul.pco.eurosplit.domain.utils.Data;

import java.util.ArrayList;


public class Expense {

    private static int numberOfExpenses;
    public static String EXPENSE_SEPARATOR = "#";

    private int id;
    private String expense;
    private int value;
    private User paidBy;
    private Data data;
    private ArrayList<User> paidFor;


    public Expense(String expense, int value, User paidBy, Data data) {
        numberOfExpenses++;
        this.id = numberOfExpenses;
        this.expense = expense;
        this.value = value;
        this.paidBy = paidBy;
        this.data = data;
        paidFor = new ArrayList<User>();
    }

    private Expense(int id, String expense, int value, User paidBy, Data data, ArrayList<User> paidFor) {
        this.id = id;
        this.expense = expense;
        this.value = value;
        this.paidBy = paidBy;
        this.data = data;
        this.paidFor = paidFor;
        numberOfExpenses++;
    }

    public int getId() {
        return id;
    }

    public String getExpense() {
        return expense;
    }

    public int getValue() {
        return value;
    }

    public User getPaidBy() {
        return paidBy;
    }

    public Data getData() {
        return data;
    }

    public ArrayList<User> getPaidFor() {
        return paidFor;
    }

    public Integer getNumberOfUsersSharingExpense() {
        return paidFor.size() + 1;
    }

    public void addPaidFor(User U) {
        paidFor.add(U);
    }

    @Override
    public String toString() {
        StringBuilder sbPaidFor = new StringBuilder();
        StringBuilder sbExpense = new StringBuilder();
        if (!paidFor.isEmpty()) {
            for (User user : paidFor) {
                sbPaidFor.append(user).append(",");
            }
            sbPaidFor.deleteCharAt(sbPaidFor.length() - 1);
        }

        return sbExpense.append(id).append(EXPENSE_SEPARATOR).append(expense).append(EXPENSE_SEPARATOR).
                append(value).append(EXPENSE_SEPARATOR).append(paidBy).append(EXPENSE_SEPARATOR).
                append(data).append(EXPENSE_SEPARATOR).append(sbPaidFor).toString();
    }

    public static Expense fromString(String s) {
        String expense[] = s.split(EXPENSE_SEPARATOR);
        ArrayList<User> paidForList = new ArrayList<>();
        if (expense.length == 6) {
            String paidFor[] = expense[5].split(",");
            for (String user : paidFor) {
                paidForList.add(User.fromString(user));
            }
        }
        return new Expense(Integer.parseInt(expense[0]), expense[1], Integer.parseInt(expense[2]),
                User.fromString(expense[3]), Data.fromString(expense[4]), paidForList);
    }
}

