package fcul.pco.eurosplit.domain;

import java.util.ArrayList;

public class Split {

    private static int numberOfSplits;
    private static String SPLIT_SEPARATOR = "#";

    private User owner;
    private int id;
    private String event;
    private final ArrayList<Expense> expenses;

    public Split(User owner, String event) {
        numberOfSplits++;
        this.id = numberOfSplits;
        this.owner = owner;
        this.event = event;
        this.expenses = new ArrayList<>();
    }

    private Split(User owner, int id, String event, ArrayList<Expense> expenses) {
        this.owner = owner;
        this.id = id;
        this.event = event;
        this.expenses = expenses;
        numberOfSplits++;
    }

    public ArrayList<Expense> getExpenses() {
        return expenses;
    }


    public void setEvent(String event) {
        this.event = event;
    }

    public User getOwner() {
        return owner;
    }

    public String getPurpose() {
        return event;
    }

    public void addExpense(Expense e) {
        expenses.add(e);
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbe = new StringBuilder();
        if (!expenses.isEmpty()) {
            for (Expense e : expenses) {
                sbe.append(e.getId()).append(":");
            }
            sbe.deleteCharAt(sbe.length() - 1);
        }
        sb.append(id).append(SPLIT_SEPARATOR).append(owner.getEmail()).append(SPLIT_SEPARATOR)
                .append(event).append(SPLIT_SEPARATOR).append(sbe);
        return sb.toString();
    }


    public static Split fromString(String s) {
        String[] split = s.split(SPLIT_SEPARATOR);
        int id = Integer.parseInt(split[0]);
        User owner = UserCatalog.getUserCatalog().getUserById(split[1]);
        String event = split[2];
        ArrayList<Expense> expenses = new ArrayList<>();
        if (split.length == 4) {
            String[] expensesIds = split[3].split(":");
            for (String expenseId : expensesIds) {
                expenses.add(ExpenseCatalog.getExpenseCatalog().getExpenseById(Integer.parseInt(expenseId)));
            }
        }
        return new Split(owner, id, event, expenses);
    }
}
