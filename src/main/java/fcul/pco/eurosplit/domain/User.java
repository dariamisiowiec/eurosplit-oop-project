package fcul.pco.eurosplit.domain;

import java.util.Objects;

public class User implements Comparable<User> {

    private String name;
    private String email;

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return name + "/" + email;
    }

    public static User fromString(String s) {
        String[] user = s.split("/");
        return new User(user[0], user[1]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(email, user.email);
    }

    //different implementation of hashcode?
    @Override
    public int hashCode() {

        return Objects.hash(name, email);
    }

    @Override
    public int compareTo(User u) {
        return name.compareTo(u.getName());
    }
}
