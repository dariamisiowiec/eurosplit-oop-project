package fcul.pco.eurosplit.domain.utils;

import java.time.LocalDateTime;

public class Data {
    private int day;
    private int month;
    private int year;
    private int hour;
    private int minute;
    private int second;

    public Data(int day, int month, int year, int hour, int minute, int second) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    @Override
    public String toString() {
        return day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
    }

    public static Data fromString(String s) {
        String[] dayHour = s.split(" ");
        String[] day = dayHour[0].split("/");
        String[] hour = dayHour[1].split(":");
        return new Data(Integer.parseInt(day[0]), Integer.parseInt(day[1]), Integer.parseInt(day[2]),
                Integer.parseInt(hour[0]), Integer.parseInt(hour[1]), Integer.parseInt(hour[2]));
    }

    public static Data now() {
        String date = LocalDateTime.now().toString();
        String[] dayHour = date.split("T");
        String[] day = dayHour[0].split("-");
        String[] hour = dayHour[1].split(":");
        return new Data(Integer.parseInt(day[0]), Integer.parseInt(day[1]), Integer.parseInt(day[2]),
                Integer.parseInt(hour[0]), Integer.parseInt(hour[1]),
                Math.round(Float.parseFloat(hour[2])));
    }


}
