package fcul.pco.eurosplit.domain;

import fcul.pco.eurosplit.persistance.ExpenseCatalogDA;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ExpenseCatalog {

    private static ExpenseCatalog expenseCatalog = null;
    Map<Integer, Expense> expenses;

    private ExpenseCatalog() {
        expenses = new HashMap<Integer, Expense>();
    }

    public static ExpenseCatalog getExpenseCatalog() {
        if (expenseCatalog == null) {
            expenseCatalog = new ExpenseCatalog();
        }
        return expenseCatalog;
    }

    public void addExpense(Expense e) {
        expenses.put(e.getId(), e);
    }

    public boolean hasExpenseWithId(Integer id) {
        if (expenses.containsKey(id)) {
            return true;
        }
        return false;
    }

    public Expense getExpenseById(Integer id) {
        if (hasExpenseWithId(id)) {
            return expenses.get(id);
        }
        return null;
    }

    public void save() throws IOException {
        ExpenseCatalogDA.save(expenses);
    }

    public void load() throws IOException {
        expenses = ExpenseCatalogDA.load();
    }

    public void getExpenses() {
        for (Integer key : expenses.keySet()) {
            System.out.println(expenses.get(key).toString());
        }
    }
}
