package fcul.pco.eurosplit.domain;

import fcul.pco.eurosplit.persistance.UserCatalogDA;

import java.io.IOException;
import java.util.*;

public class UserCatalog {
    public static UserCatalog userCatalog = null;
    private Map<String, User> users;

    private UserCatalog() {
        users = new HashMap<String, User>();
    }

    public static UserCatalog getUserCatalog() {
        if (userCatalog == null) {
            userCatalog = new UserCatalog();
        }
        return userCatalog;
    }

    public void addUser(User u) {
        users.put(u.getEmail(), u);
    }

    public boolean hasUserWithId(String email) {
        if (users.containsKey(email)) {
            return true;
        }
        return false;
    }

    public boolean hasUserWithName(String name) {
        for (User user : users.values()) {
            if (user.getName().equals(name) || user.getName().toLowerCase().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public User getUserById(String id) {
        if (hasUserWithId(id)) {
            return users.get(id);
        }
        return null;
    }

    public ArrayList<User> getUsersWithName(String name) {
        ArrayList<User> foundUsers = new ArrayList<>();
        for (User user : users.values()
                ) {
            if (user.getName().equals(name)) {
                foundUsers.add(user);
            }
        }
        return foundUsers;
    }


    public void save() throws IOException {
        UserCatalogDA.save(users);
    }


    public void load() throws IOException {
        users = UserCatalogDA.load();
    }

    public List<List<String>> getUsers() {
        List<List<String>> superList = new ArrayList<>();
        List<User> list = new ArrayList<>();
        list.addAll(users.values());
        Collections.sort(list);
        for (User u : list) {
            List<String> userdata = new ArrayList<>();
            userdata.add(u.getName());
            userdata.add(u.getEmail());
            superList.add(userdata);
        }
        return superList;
    }
}
